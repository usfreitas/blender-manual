.. _composite-nodes-index:
.. _bpy.types.Compositor:

###############
  Compositing
###############

.. toctree::
   :maxdepth: 2

   introduction.rst
   sidebar.rst
   realtime_compositor.rst

.. index:: Compositor Nodes

.. _compositor-nodes:

Node Types
==========

.. toctree::
   :maxdepth: 1

   types/input/index.rst
   types/output/index.rst

----------

.. toctree::
   :maxdepth: 1

   types/color/index.rst
   types/filter/index.rst

----------

.. toctree::
   :maxdepth: 1

   types/keying/index.rst
   types/mask/index.rst

----------

.. toctree::
   :maxdepth: 1

   types/tracking/index.rst

----------

.. toctree::
   :maxdepth: 1

   types/transform/index.rst
   types/utilities/index.rst
   types/vector/index.rst

----------

.. toctree::
   :maxdepth: 1

   types/groups.rst
   types/layout/index.rst
